package client;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.TitledBorder;

public class GUI {
	
	JFrame f;
	JFrame settings;
	JFrame teamquery;
	JPanel idPanel;
	JPanel autoPanel;
	JPanel teleopPanel;
	
	GridBagConstraints c = new GridBagConstraints();
	
	JButton settings_b = new JButton("Settings");
	JButton submit = new JButton("Submit Data");
	JButton teamData = new JButton("Team Data");
	public static JButton connection_b = new JButton("Connect");
	public static JButton disconnect_b = new JButton("Disconnect");
	public static JLabel connection_status = new JLabel("not connected");
	
	//query components
	JButton requestData = new JButton("Request Data");
	JTextField queryTeam = new JTextField(4);
	JTable datatable = new JTable();
	
	//identifiers
		JRadioButton rb_blue = new JRadioButton("blue");
		JRadioButton rb_red = new JRadioButton("red");
		
		JRadioButton rb_quals = new JRadioButton("Qualification");
		JRadioButton rb_elims = new JRadioButton("Elimination");
		
		ButtonGroup match_type = new ButtonGroup();
		ButtonGroup aliance_colors = new ButtonGroup();
		ButtonGroup aim_group = new ButtonGroup();
		
		JTextField team_number = new JTextField(4);
		JTextField match_number = new JTextField(3);
		JLabel team_lbl = new JLabel("Team Number");
		JLabel match_lbl = new JLabel("Match Number");
		
	//auto panel components
		JCheckBox has_auto = new JCheckBox("Auto", true);
		JCheckBox ball_detection = new JCheckBox("Ball Detection");
		JCheckBox hot_goal_detection = new JCheckBox("Hot Goal Detection");
		JCheckBox zone_move = new JCheckBox("Moved to colored zone");
		JTextField auto_score = new JTextField(6);
		JTextField balls_attempted = new JTextField(2);
		JTextField balls_made = new JTextField(2);
		JTextField hot_goals = new JTextField(2);
		JLabel aim_lbl = new JLabel("Aimed for:");
		JRadioButton aim_high = new JRadioButton("High Goal");
		JRadioButton aim_low = new JRadioButton("Low Goal");
	
	//teleop panel components
		JLabel assist_lbl = new JLabel("Assists: 0");
		JLabel high_goal_lbl = new JLabel("High Goals: 0");
		JLabel hga_lbl = new JLabel("High Goals Attempted: 0");
		JLabel low_goal_lbl = new JLabel("Low Goals: 0");
		JLabel lga_lbl = new JLabel("Low Goals Attempted: 0");
		JLabel truss_shot_lbl = new JLabel("Truss Shots: 0");
		JLabel block_lbl = new JLabel("Blocks: 0");
		JLabel catch_lbl = new JLabel("Catches: 0");
		JLabel foul_lbl = new JLabel("Fouls: 0");
		JLabel tech_foul_lbl = new JLabel("Tech Fouls: 0");
		JRadioButton rb_offense = new JRadioButton("Offensive");
		JRadioButton rb_defense = new JRadioButton("Defensive");
		ButtonGroup play_style_group = new ButtonGroup();
		
		//increment buttons
		JButton assist_p = new JButton("+");
		JButton assist_m = new JButton("-");
		JButton hg_m = new JButton("-");
		JButton hg_p = new JButton("+");
		JButton hga_m = new JButton("-");	
		JButton hga_p = new JButton("+");
		JButton lg_m = new JButton("-");
		JButton lg_p = new JButton("+");
		JButton lga_m = new JButton("-");
		JButton lga_p = new JButton("+");
		JButton truss_p = new JButton("+");
		JButton truss_m = new JButton("-");
		JButton block_p = new JButton("+");
		JButton block_m = new JButton("-");
		JButton catch_p = new JButton("+");
		JButton catch_m = new JButton("-");
		JButton foul_p = new JButton("+");
		JButton foul_m = new JButton("-");
		JButton TF_p = new JButton("+");
		JButton TF_m = new JButton("-");
	
	//settings Components
		JLabel ip_lbl = new JLabel("Server IP:");
		JLabel port_lbl = new JLabel("Server port:");
		JTextField ip_field = new JTextField("192.168.1.10", 10);
		JTextField port_field = new JTextField("4265", 10);
		JButton save_settings = new JButton("Save Settings");
		
	public GUI(){
		f = new JFrame("Scouting Client");
		f.setLayout(new GridBagLayout());
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		settings = new JFrame("Settings");
		settings.setLayout(new GridBagLayout());
		settings.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		teamquery = new JFrame("Team Data Request");
		teamquery.setLayout(new GridBagLayout());
		teamquery.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		TitledBorder IdentificationBorder = BorderFactory.createTitledBorder("Identification");
		TitledBorder autoBorder = BorderFactory.createTitledBorder("Autonomous");
		TitledBorder teleopBorder = BorderFactory.createTitledBorder("Teleoperated");
		
		autoPanel = new JPanel(new GridBagLayout());
		autoPanel.setBorder(autoBorder);
		
		teleopPanel = new JPanel(new GridBagLayout());
		teleopPanel.setBorder(teleopBorder);
		teleopPanel.setVisible(true);
		
		idPanel = new JPanel(new GridBagLayout());
		idPanel.setBorder(IdentificationBorder);
		

		
		//setup ID Panel
		c.gridx = 0;
		c.gridy = 0;
		c.fill = c.HORIZONTAL;
		c.insets = new Insets(0,20,0,20);
		JLabel tn_lab = new JLabel("Team Number");
		tn_lab.setHorizontalAlignment(JLabel.CENTER);
		idPanel.add(tn_lab, c);
		c.gridx = 0;
		c.gridy = 1;
		idPanel.add(team_number, c);
		team_number.setText("1");
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 2;
		c.ipadx = 20;
		idPanel.add(new JLabel("Alliance Color"), c);
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 1;
		c.ipadx = 0;
		c.insets = new Insets(0,0,0,0);
		idPanel.add(rb_red, c);
		c.gridx = 2;
		c.gridy = 1;
		idPanel.add(rb_blue, c);
		c.gridx = 3;
		c.gridy = 0;
		idPanel.add(match_lbl, c);
		c.gridy = 1;
		idPanel.add(match_number, c);
		match_number.setText("1");
		JLabel mt_lbl = new JLabel("Match Type");
		mt_lbl.setHorizontalAlignment(JLabel.CENTER);
		
		c.gridx = 4;
		c.gridy = 0;
		c.gridwidth = 2;
		idPanel.add(mt_lbl, c);
		c.gridy = 1;
		c.gridwidth = 1;
		idPanel.add(rb_quals, c);
		c.gridx = 5;
		idPanel.add(rb_elims, c);
		
		//setup auto panel
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.weightx = 0.5;
		c.insets = new Insets(0,0,0,0);
		c.fill = c.NONE;
		c.anchor = c.WEST;
		autoPanel.add(aim_lbl,c);
		
		c.gridy = 1;
		autoPanel.add(aim_high,c);
		
		c.gridy = 2;
		autoPanel.add(aim_low,c);
		
		c.gridx = 2;
		c.gridy = 0;
		c.insets = new Insets(0,50,0,0);
		autoPanel.add(hot_goal_detection, c);
		
		c.gridy = 1;
		autoPanel.add(ball_detection, c);
		
		c.gridy = 2;
		autoPanel.add(zone_move, c);
		
		c.insets = new Insets(0,10,0,10);
		c.gridx = 0;
		c.gridy = 3;
		c.fill = c.NONE;
		c.gridwidth = 1;
		autoPanel.add(new JLabel("Balls Attempted:"), c);

		c.gridx = 1;
		autoPanel.add(balls_attempted, c);
		balls_attempted.setText("0");
		
		c.gridy = 4;
		c.gridx = 0;
		c.anchor = c.WEST;
		autoPanel.add(new JLabel("Balls Made:"), c);
						
		c.gridx = 1;
		autoPanel.add(balls_made, c);
		balls_made.setText("0");
		
		c.gridy = 5;
		c.gridx = 0;
		autoPanel.add(new JLabel("Hot Goals:"), c);
		
		c.gridx = 1;
		autoPanel.add(hot_goals, c);
		hot_goals.setText("0");
		
		c.gridx = 2;
		c.gridy = 3;
		c.gridwidth = 2;
		autoPanel.add(new JLabel("Total Auto Score:"), c);
		
		c.gridy = 4;
		autoPanel.add(auto_score, c);
		auto_score.setText("0");
		
		//setup teleop panel
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.gridheight = 1;
		c.fill = c.NONE;
		c.anchor = c.WEST;
		c.insets = new Insets(5,2,5,2);
		c.weightx = 0;
		teleopPanel.add(lg_m, c);
		
		c.gridy = 1;
		teleopPanel.add(lga_m, c);
		
		c.gridy = 2;
		teleopPanel.add(truss_m, c);

		c.gridy = 3;
		teleopPanel.add(assist_m, c);
		
		c.gridy = 4;
		teleopPanel.add(foul_m, c);
		
		c.gridx = 1;
		c.gridy = 0;
		teleopPanel.add(low_goal_lbl, c);
		
		c.gridy = 1;
		teleopPanel.add(lga_lbl, c);
		
		c.gridy = 2;
		teleopPanel.add(truss_shot_lbl, c);
		
		c.gridy = 3;
		teleopPanel.add(assist_lbl, c);
		
		c.gridy = 4;
		teleopPanel.add(foul_lbl, c);
		
		c.gridx = 2;
		c.gridy = 0;
		c.insets = new Insets(5,2,5,20);
		teleopPanel.add(lg_p, c);
		
		c.gridy = 1;
		teleopPanel.add(lga_p, c);
		
		c.gridy = 2;
		teleopPanel.add(truss_p, c);
		
		c.gridy = 3;
		teleopPanel.add(assist_p, c);
		
		c.gridy = 4;
		teleopPanel.add(foul_p, c);
		
		c.gridx = 3;
		c.gridy = 0;
		c.insets = new Insets(5,20,5,2);
		teleopPanel.add(hg_m, c);
		
		c.gridy = 1;
		teleopPanel.add(hga_m, c);
		
		c.gridy = 2;
		teleopPanel.add(catch_m, c);
		
		c.gridy = 3;
		teleopPanel.add(block_m, c);
		
		c.gridy = 4;
		teleopPanel.add(TF_m, c);
		
		c.gridx = 4;
		c.gridy = 0;
		c.insets = new Insets(5,2,5,2);
		teleopPanel.add(high_goal_lbl, c);
		
		c.gridy = 1;
		teleopPanel.add(hga_lbl, c);
		
		c.gridy = 2;
		teleopPanel.add(catch_lbl, c);
		
		c.gridy = 3;
		teleopPanel.add(block_lbl, c);
		
		c.gridy = 4;
		teleopPanel.add(tech_foul_lbl, c);
		
		c.gridx = 5;
		c.gridy = 0;
		teleopPanel.add(hg_p, c);
		
		c.gridy = 1;
		teleopPanel.add(hga_p, c);
		
		c.gridy = 2;
		teleopPanel.add(catch_p, c);
		
		c.gridy = 3;
		teleopPanel.add(block_p, c);
		
		c.gridy = 4;
		teleopPanel.add(TF_p, c);
		
		
		JPanel stylePanel = new JPanel(new GridBagLayout());
		stylePanel.setBorder(BorderFactory.createTitledBorder("Play Style"));
		c.gridx = 0;
		c.gridy = 0;
		stylePanel.add(rb_offense, c);
		
		c.gridx = 1;
		stylePanel.add(rb_defense, c);
		
		c.gridx = 0;
		c.gridy = 5;
		c.gridwidth = 6;
		c.fill = c.HORIZONTAL;
		teleopPanel.add(stylePanel, c);
		
		
		//setup frame
		JPanel connectionPanel = new JPanel(new GridBagLayout());
		c.gridx = 0;
		c.gridy = 0;
		c.fill = c.NONE;
		c.anchor = c.WEST;
		c.gridwidth = 1;
		c.weightx = 1;
		connectionPanel.add(connection_b, c);
		connectionPanel.add(disconnect_b, c);
		disconnect_b.setVisible(false);
		
		c.gridx = 1;
		c.weightx = 0;
		connectionPanel.add(connection_status, c);
		
		c.gridx = 2;
		c.anchor = c.EAST;
		c.weightx = 1;
		connectionPanel.add(settings_b, c);
		
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 2;
		c.anchor = c.CENTER;
		c.fill = c.BOTH;
		c.insets = new Insets(0,0,0,0);
		f.add(connectionPanel, c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.fill = c.HORIZONTAL;
		c.ipadx = 10;
		c.ipady = 10;
		c.gridwidth = 2;
		c.anchor = c.CENTER;
		f.add(idPanel, c);
		
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 1;
		f.add(has_auto, c);
		
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 2;
		c.fill = c.BOTH;
		f.add(autoPanel, c);
		
		c.gridy = 4;
		f.add(teleopPanel, c);
		
		c.gridy = 5;
		c.fill = c.NONE;
		c.insets = new Insets(10,0,10,0);
		c.gridwidth = 1;
		f.add(submit, c);
		
		c.gridx = 1;
		f.add(teamData, c);
		
		//setup settings
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		c.insets = new Insets(4,0,4,0);
		settings.add(ip_lbl, c);
		
		c.gridy = 1;
		settings.add(port_lbl, c);
		
		c.gridx = 1;
		c.gridy = 0;
		settings.add(ip_field, c);
		
		c.gridy = 1;
		settings.add(port_field, c);
		
		c.gridx = 0;
		c.gridy = 2;
		c.gridwidth = 2;
		c.fill = c.NONE;
		settings.add(save_settings, c);
		
		//setup team data query frame
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 1;
		teamquery.add(queryTeam,c);
		
		c.gridx = 1;
		teamquery.add(requestData, c);
		
		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 2;
		teamquery.add(datatable, c);
		datatable.setVisible(false);
		
		//post layout setup
		aliance_colors.add(rb_blue);
		aliance_colors.add(rb_red);
		
		match_type.add(rb_quals);
		match_type.add(rb_elims);
		rb_quals.setSelected(true);
		
		aim_group.add(aim_high);
		aim_group.add(aim_low);
		
		play_style_group.add(rb_offense);
		play_style_group.add(rb_defense);
		
		
		setupListeners();
		
		f.pack();
		f.setVisible(true);
		
	}
	private void setupListeners(){
		rb_blue.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Data.teamColor = Data.TeamColor.BLUE;
			}
		});
		rb_red.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Data.teamColor = Data.TeamColor.RED;
			}
		});
		rb_quals.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Data.matchType = Data.MatchType.QUALS;
			}
		});
		rb_elims.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Data.matchType = Data.MatchType.ELIMS;
			}
		});
		rb_offense.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Data.play_style = Data.Style.OFFENSE;
			}
		});
		rb_defense.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Data.play_style = Data.Style.DEFENSE;
			}
		});
		aim_high.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Data.goal_aim = Data.Goal.HIGH;
			}
		});
		aim_low.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				Data.goal_aim = Data.Goal.LOW;
			}
		});
		has_auto.addItemListener(new ItemListener(){
			public void itemStateChanged(ItemEvent e){
				if(e.getStateChange() == ItemEvent.DESELECTED){
					autoPanel.setVisible(false);
					Data.has_auto = false;
				}else{
					autoPanel.setVisible(true);
					Data.has_auto = true;
				}
				f.pack();
			}
		});
		settings_b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				settings.pack();
				settings.setVisible(true);
				Point fLoc = f.getLocationOnScreen();
				settings.setLocation(new Point(fLoc.x+100, fLoc.y+100));
			}
		});
		save_settings.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				
				String ip = ip_field.getText();
				int port = Integer.parseInt(port_field.getText());
				
				NetClient.server_ip = ip;
				NetClient.server_port = port;
				settings.setVisible(false);
				
			}
		});
		
		ActionListener incrementorListener = new ActionListener(){
			public void actionPerformed(ActionEvent e){
				if(e.getSource() == lg_m){
					Data.low_goals--;
					low_goal_lbl.setText("Low Goals: " + Data.low_goals);
				}
				if(e.getSource() == lg_p){
					Data.low_goals++;
					low_goal_lbl.setText("Low Goals: " + Data.low_goals);
				}
				if(e.getSource() == lga_m){
					Data.low_goals_attempted--;
					lga_lbl.setText("Low Goals Attempted: " + Data.low_goals_attempted);
				}
				if(e.getSource() == lga_p){
					Data.low_goals_attempted++;
					lga_lbl.setText("Low Goals Attempted: " + Data.low_goals_attempted);
				}
				if(e.getSource() == hg_m){
					Data.high_goals--;
					high_goal_lbl.setText("High Goals: " + Data.high_goals);
				}
				if(e.getSource() == hg_p){
					Data.high_goals++;
					high_goal_lbl.setText("High Goals: " + Data.high_goals);
				}
				if(e.getSource() == hga_m){
					Data.high_goals_attempted--;
					hga_lbl.setText("High Goals Attempted: " + Data.high_goals_attempted);
				}
				if(e.getSource() == hga_p){
					Data.high_goals_attempted++;
					hga_lbl.setText("High Goals Attempted: " + Data.high_goals_attempted);
				}
				if(e.getSource() == truss_m){
					Data.truss_shots--;
					truss_shot_lbl.setText("Truss Shots: " + Data.truss_shots);
				}
				if(e.getSource() == truss_p){
					Data.truss_shots++;
					truss_shot_lbl.setText("Truss Shots: " + Data.truss_shots);
				}
				if(e.getSource() == catch_m){
					Data.catches--;
					catch_lbl.setText("Catches: " + Data.catches);
				}
				if(e.getSource() == catch_p){
					Data.catches++;
					catch_lbl.setText("Catches: " + Data.catches);
				}
				if(e.getSource() == assist_m){
					Data.assists--;
					assist_lbl.setText("Assists: " + Data.assists);
				}
				if(e.getSource() == assist_p){
					Data.assists++;
					assist_lbl.setText("Assists: " + Data.assists);
				}
				if(e.getSource() == block_m){
					Data.blocks--;
					block_lbl.setText("Blocks: " + Data.blocks);
				}
				if(e.getSource() == block_p){
					Data.blocks++;
					block_lbl.setText("Blocks: " + Data.blocks);
				}
				if(e.getSource() == foul_m){
					Data.fouls--;
					foul_lbl.setText("Fouls: " + Data.fouls);
				}
				if(e.getSource() == foul_p){
					Data.fouls++;
					foul_lbl.setText("Fouls: " + Data.fouls);
				}
				if(e.getSource() == TF_m){
					Data.tech_fouls--;
					tech_foul_lbl.setText("Tech Fouls: \n" + Data.tech_fouls);
				}
				if(e.getSource() == TF_p){
					Data.tech_fouls++;
					tech_foul_lbl.setText("Tech Fouls: " + Data.tech_fouls);
				}
			}
		};
		
		lg_m.addActionListener(incrementorListener);
		lg_p.addActionListener(incrementorListener);
		hg_m.addActionListener(incrementorListener);
		hg_p.addActionListener(incrementorListener);
		lga_m.addActionListener(incrementorListener);
		lga_p.addActionListener(incrementorListener);
		hga_m.addActionListener(incrementorListener);
		hga_p.addActionListener(incrementorListener);
		truss_m.addActionListener(incrementorListener);
		truss_p.addActionListener(incrementorListener);
		catch_m.addActionListener(incrementorListener);
		catch_p.addActionListener(incrementorListener);
		assist_m.addActionListener(incrementorListener);
		assist_p.addActionListener(incrementorListener);
		block_m.addActionListener(incrementorListener);
		block_p.addActionListener(incrementorListener);
		foul_m.addActionListener(incrementorListener);
		foul_p.addActionListener(incrementorListener);
		TF_m.addActionListener(incrementorListener);
		TF_p.addActionListener(incrementorListener);
		
		submit.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				
				Data.auto_score = Integer.parseInt(auto_score.getText());
				Data.balls_attempted = Integer.parseInt(balls_attempted.getText());
				Data.balls_made = Integer.parseInt(balls_made.getText());
				Data.hot_goals = Integer.parseInt(hot_goals.getText());
				Data.matchNumber = Integer.parseInt(match_number.getText());
				Data.teamNumber = Integer.parseInt(team_number.getText());
				Data.ball_detection = ball_detection.isSelected();
				Data.has_auto = has_auto.isSelected();
				Data.hot_goal_detection = hot_goal_detection.isSelected();
				Data.zone_move = zone_move.isSelected();
				Data.calculate();
				if(NetClient.isConnected())
					NetClient.send();
				
				
			}
		});
		
		teamData.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				teamquery.setSize(250,600);
				Point fLoc = f.getLocationOnScreen();
				teamquery.setLocation(new Point(fLoc.x+100, fLoc.y+100));
				teamquery.setVisible(true);
			}
		});
		requestData.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				int team = Integer.parseInt(queryTeam.getText());
				String[][] tabledata = NetClient.request(team);
				teamquery.remove(datatable);
				datatable = new JTable(tabledata, new String[] {"", ""});
				c.gridx = 0;
				c.gridy = 1;
				c.gridwidth = 2;
				c.fill = c.HORIZONTAL;
				teamquery.add(datatable, c);
				
				datatable.setVisible(true);
			}
		});
		connection_b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				NetClient.Connect();
			}
		});
		disconnect_b.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				NetClient.disconnect();
			}
		});
	}
	
}
