package client;

public class Data {
	
	//metadata
	public static enum TeamColor {RED, BLUE};
	public static enum MatchType {QUALS, ELIMS};
	public static int teamNumber = 0;
	public static TeamColor teamColor;
	public static MatchType matchType = MatchType.QUALS; 
	public static int matchNumber = 0;
	
	public static enum Goal {HIGH, LOW, NA};
	
	//autonomous
	public static boolean has_auto;
	public static boolean ball_detection;
	public static boolean hot_goal_detection;
	public static boolean zone_move;
	public static Goal goal_aim = Goal.NA;
	public static int balls_attempted;
	public static int balls_made;
	public static int hot_goals;
	public static int auto_score;
	public static double auto_accuracy;
	
	//teleop
	public static enum Style {OFFENSE, DEFENSE};
	public static int assists = 0;
	public static int high_goals = 0;
	public static int high_goals_attempted = 0;
	public static double high_goal_accuracy;
	
	public static int low_goals = 0;
	public static int low_goals_attempted = 0;
	public static double low_goal_accuracy;
	
	public static int truss_shots = 0;
	public static int catches = 0;
	public static int blocks = 0;
	public static Style play_style;
	public static int fouls = 0;
	public static int tech_fouls = 0;

	public static String message;
	
	public static void calculate(){
		auto_accuracy = Double.isNaN(balls_made*1.0/balls_attempted) ? 0 : (balls_made+1.0/balls_attempted);
		high_goal_accuracy = Double.isNaN(high_goals*1.0/high_goals_attempted) ? 0 : (high_goals*1.0/high_goals_attempted);
		low_goal_accuracy = Double.isNaN(low_goals*1.0/low_goals_attempted) ? 0 : (low_goals*1.0/low_goals_attempted);
		message = ""+teamNumber+':'+matchNumber+':'+teamColor+':'+matchType+':'+
				has_auto+':'+ball_detection+':'+hot_goal_detection+':'+zone_move+':'+
				goal_aim+':'+balls_attempted+':'+balls_made+':'+auto_accuracy+':'+
				auto_score+':'+assists+':'+high_goals+':'+high_goals_attempted+':'+
				high_goal_accuracy+':'+low_goals+':'+low_goals_attempted+':'+low_goal_accuracy+
				':'+hot_goals+':'+truss_shots+':'+catches+':'+blocks+':'+play_style+':'+fouls+':'+tech_fouls+':';
	}

}
