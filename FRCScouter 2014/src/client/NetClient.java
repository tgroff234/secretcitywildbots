package client;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class NetClient {
	
	public static String server_ip = "192.168.1.10";
	public static int server_port = 4265;
	private static boolean connected = false;
	private static int last_match_submitted = 0;
	
	private static PrintWriter writer;
	private static BufferedReader reader;

	private static Socket socket = new Socket();
	
	public NetClient(){
		
	}
	public static void Connect(){
		GUI.connection_status.setText("Connecting...");
		InetAddress ip_addr = null;
		try {
			ip_addr = InetAddress.getByName(server_ip);
			
		} catch (UnknownHostException e) {
			connected = false;
			GUI.connection_status.setText("Invalid IP address. check settings");
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		InetSocketAddress socket_addr = new InetSocketAddress(ip_addr, server_port); 
		try {
			socket.connect(socket_addr);
			connected = true;
			GUI.connection_status.setText("Connected to "+server_ip+" on port "+server_port);
			GUI.connection_b.setVisible(false);
			GUI.disconnect_b.setVisible(true);
			writer = new PrintWriter(socket.getOutputStream());
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			
		} catch (IOException e) {
			connected = false;
			GUI.connection_status.setText("Could not connect to server");
			// TODO Auto-generated catch block
			e.printStackTrace();
			socket = new Socket();
			return;
		}
		
	}
	public static void disconnect(){
		try {
			socket.close();
			GUI.disconnect_b.setVisible(false);
			GUI.connection_b.setVisible(true);
			GUI.connection_status.setText("Not Connected");
			socket = new Socket();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static boolean isConnected(){
		return connected;
	}
	public static void send(){
		if(Data.matchNumber > last_match_submitted){
			int messagelen = Data.message.length();
			String smesglen = messagelen+"";
			if(smesglen.length() == 2)
				smesglen = "0"+smesglen;
			String message = String.format("d%s%s",smesglen, Data.message );
			writer.print(message);
			writer.flush();
			System.out.println(message);
		}
	}
	public static String[][] request(int team){
		String Steam = team+"";
		String teamString = team+"";
		String dataString = "";
		String[] data = null;
		String[][] table = new String[33][2];
		for(int i = 0; i < 4-Steam.length(); i ++){
			teamString = "0"+teamString;
		}
		writer.printf("q%s", teamString);
		writer.flush();
		
		try{
			while(!reader.ready());
			dataString = reader.readLine();
			data = dataString.split(":");
			table[0][0] = "Team";
			table[1][0] = "Has Auto";
			table[2][0] = "Ball Detection";
			table[3][0] = "Hot Goal Detection";
			table[4][0] = "Moves to colored zone";
			table[5][0] = "Auto Goal Target";
			table[6][0] = "Average Auto Balls Attempted";
			table[7][0] = "Average Auto Balls Made";
			table[8][0] = "Average Auto Accuracy";
			table[9][0] = "Average Hot Goals Made";
			table[10][0] = "Average Auto Score";
			table[11][0] = "Play Style";
			table[12][0] = "Average High Goals";
			table[13][0] = "Most High Goals";
			table[14][0] = "Least High Goal Accuracy";
			table[15][0] = "Average High Goal Accuracy";
			table[16][0] = "Most High Goal Accuracy";
			table[17][0] = "Average Low Goals";
			table[18][0] = "Most Low Goals";
			table[19][0] = "Least Low Goal Accuracy";
			table[20][0] = "Average Low Goal Accuracy";
			table[21][0] = "Most Low Goal Accuracy";
			table[22][0] = "Average Assists";
			table[23][0] = "Most Assists";
			table[24][0] = "Average Truss Shots";
			table[25][0] = "Most Truss Shots";
			table[26][0] = "Total Catches";
			table[27][0] = "Percentage Offensive";
			table[28][0] = "Percentage Defensive";
			table[29][0] = "Percentage High Goals";
			table[30][0] = "Percentage Low Goals";
			table[31][0] = "Total Fouls";
			table[32][0] = "Total Technical Fouls";
			table[0][1] = data[0];
			table[1][1] = data[1];
			table[2][1] = data[2];
			table[3][1] = data[3];
			table[4][1] = data[4];
			table[5][1] = data[5];
			table[6][1] = data[6];
			table[7][1] = data[7];
			table[8][1] = data[8];
			table[9][1] = data[9];
			table[10][1] = data[10];
			table[11][1] = data[11];
			table[12][1] = data[12];
			table[13][1] = data[13];
			table[14][1] = data[14];
			table[15][1] = data[15];
			table[16][1] = data[16];
			table[17][1] = data[17];
			table[18][1] = data[18];
			table[19][1] = data[19];
			table[20][1] = data[20];
			table[21][1] = data[21];
			table[22][1] = data[22];
			table[23][1] = data[23];
			table[24][1] = data[24];
			table[25][1] = data[25];
			table[26][1] = data[26];
			table[27][1] = data[27];
			table[28][1] = data[28];
			table[29][1] = data[29];
			table[30][1] = data[30];
			table[31][1] = data[31];
			table[32][1] = data[32];
		}catch(IOException e){
			e.printStackTrace();
		}
		return table;
	}
	
}
